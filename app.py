import os
from flask import Flask, request, abort

from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,
)

app = Flask(__name__)

line_bot_api = LineBotApi('OIWM0g0VvzEfPB2tS39MNBpQnyZyPoSpjS3raaCSVahuFbfd14JwrutMR2ka9SbA0xvOtieLjwjQkn/6PjvWL6bTiMS0I/u1xQrYafFHlYd0uUkiLKsgReIHjcdOCte2/U+y2UaZMCW8QVZ158dOyAdB04t89/1O/w1cDnyilFU=')
handler = WebhookHandler('2d55604d56f4070ed166468dd8236ede')


@app.route("/callback", methods=['POST'])
def callback():
    # Get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']

    # Get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)

    # Handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)

    return 'OK'


@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    """ Here's all the messages will be handled and processed by the program """

    msg = (event.message.text).lower()

    if "hello" in msg:
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text="Hello Pengguna"))

    else:
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text=event.message.text))


if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
